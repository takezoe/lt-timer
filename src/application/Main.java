package application;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Main extends Application {
    private static final List<String> SOUND_NAMES = Arrays.asList("start", "timeup", "remaining10m");

    private static final int TIMER_INTERVAL = 1000;

    @FXML
    private Button okButton;

    @FXML
    private TextField minuteField;

    @FXML
    private TextField secondField;

    private IntegerProperty remainingSecond = new SimpleIntegerProperty(0);

    private Map<String, MediaPlayer> soundPlayers = new HashMap<String, MediaPlayer>();

    private Timeline timer;

    @FXML
    private Button stopButton;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void init() {
        System.out.println("init");
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            System.out.println("start");
            Parent root = FXMLLoader.load(getClass().getResource("./main.fxml"));
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void initialize() {
        System.out.println("initialize");

        timer = new Timeline(new KeyFrame(Duration.millis(TIMER_INTERVAL), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (remainingSecond.get() < 1) {
                    timer.stop();
                    return;
                }
                remainingSecond.set(remainingSecond.get() - 1);
            }
        }));
        timer.setCycleCount(Timeline.INDEFINITE);

        remainingSecond.addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                secondField.setText(Integer.toString(newValue.intValue() % 60));
                minuteField.setText(Integer.toString((int) newValue.doubleValue() / 60));
                if (newValue.equals(601)) {
                    soundPlayers.get("remaining10m").stop();
                    soundPlayers.get("remaining10m").play();
                    System.out.println("あと１0分です。");
                }
                if (newValue.equals(0)) {
                    soundPlayers.get("timeup").stop();
                    soundPlayers.get("timeup").play();

                    setEnableToInput();
                    setEnableToStartCountdown();
                }
            }
        });
        initSoundPlayers();
    }

    private void initSoundPlayers() {
        SOUND_NAMES.forEach(new Consumer<String>() {
            @Override
            public void accept(String soundName) {
                MediaPlayer player = new MediaPlayer(
                        new Media(getClass().getResource("./" + soundName + ".wav").toString()));
                player.setCycleCount(1);
                soundPlayers.put(soundName, player);
            }
        });
    }

    @FXML
    protected void onClickOkButton(ActionEvent event) {
        // TODO validate input
        int minute = Integer.parseInt(minuteField.getText());
        int second = Integer.parseInt(secondField.getText());
        remainingSecond.set(minute * 60 + second);
        setDisableToInput();
        setDisableToStartCountdown();
        timer.play();
        soundPlayers.get("start").stop();
        soundPlayers.get("start").play();
    }

    @FXML
    public void onClickStopButton() {
        timer.stop();
        setEnableToInput();
        setEnableToStartCountdown();
    }

    private void setDisableToInput() {
        minuteField.setEditable(false);
        secondField.setEditable(false);
    }

    private void setEnableToInput() {
        minuteField.setEditable(true);
        secondField.setEditable(true);
    }

    private void setDisableToStartCountdown() {
        stopButton.setDisable(false);
        okButton.setDisable(true);
    }

    private void setEnableToStartCountdown() {
        stopButton.setDisable(true);
        okButton.setDisable(false);
    }
}
